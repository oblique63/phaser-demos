# Based on this tutorial:
# https://gamedevacademy.org/create-a-dialog-modal-plugin-in-phaser-3-part-1/
# https://gamedevacademy.org/create-a-dialog-modal-plugin-in-phaser-3-part-2/

class @DialogModalPlugin extends Phaser.Plugins.ScenePlugin
  constructor: (scene, pluginManager) ->
    super(scene, pluginManager)

  # Called when the plugin is loaded by the PluginManager
  boot: ->
    eventEmitter = @systems.events
    eventEmitter.on('shutdown', @shutdown, this)
    eventEmitter.on('destroy',  @destroy,  this)

  shutdown: ->
    @timedEvent.remove() if @timedEvent?
    @text.destroy() if @text?

  destroy: ->
    @shutdown()
    @scene = undefined

  init: (opts={}) ->
    @borderThickness = opts.borderThickness ? 3
    @borderColor     = opts.borderColor ? 0x907748
    @borderAlpha     = opts.borderAlpha ? 1
    @windowAlpha     = opts.windowAlpha ? 0.8
    @windowColor     = opts.windowColor ? 0x303030
    @windowHeight    = opts.windowHeight ? 150
    @padding         = opts.padding ? 32
    @closeBtnColor   = opts.closeBtnColor ? 'darkgoldenrod'
    @dialogSpeed     = opts.dialogSpeed ? 3

    @visible      = true
    @eventCounter = 0
    @graphics     = null
    @closeBtn     = null

    # The current text in the window
    @text = null
    # The text that will be displayed in the window
    @dialog = null

    # Create the dialog window
    @_createWindow()

  toggleWindow: ->
    @visible = not @visible
    if @text?     then @text.visible     = @visible
    if @graphics? then @graphics.visible = @visible
    if @closeBtn? then @closeBtn.visible = @visible

  closeWindow: ->
    if @visible
      @toggleWindow()
      @shutdown()

  setText: (text, animate=false) ->
    @eventCounter = 0
    @dialog = text.split('')

    @timedEvent.remove() if @timedEvent?

    tempText = if animate then '' else text
    @_setText(tempText)

    if animate
      @timedEvent = @scene.time.addEvent
        delay: 150 - (@dialogSpeed * 30)
        callback: @_animateText
        callbackScope: this
        loop: true

  _setText: (text) ->
    x = @padding + 10
    y = @_getGameHeight() - @windowHeight - @padding + 10

    @text.destroy() if @text?
    @text = @scene.make.text
        x: x
        y: y
        text: text
        style:
          wordWrap: { width: @_getGameWidth() - (@padding * 2) - 25 }

  _animateText: ->
    @eventCounter++
    @text.setText(@text.text + @dialog[@eventCounter - 1])
    if @eventCounter == @dialog.length
      @timedEvent.remove()

  _getGameWidth: ->
    @scene.sys.game.config.width

  _getGameHeight: ->
    @scene.sys.game.config.height

  _calculateWindowDimensions: (width, height) ->
    x = @padding
    y = height - @windowHeight - @padding

    rectWidth  = width - (@padding * 2)
    rectHeight = @windowHeight

    [x, y, rectWidth, rectHeight]

  # Creates the inner dialog window where the text is displayed
  _createInnerWindow: (x, y, rectWidth, rectHeight) ->
    @graphics.fillStyle(@windowColor, @windowAlpha)
    @graphics.fillRect(x + 1, y + 1, rectWidth - 1, rectHeight - 1)

  # Creates the border rectangle of the dialog window
  _createOuterWindow: (x, y, rectWidth, rectHeight) ->
    @graphics.lineStyle(@borderThickness, @borderColor, @borderAlpha)
    @graphics.strokeRect(x, y, rectWidth, rectHeight)

  _createCloseModalButtonBorder: ->
    x = @_getGameWidth() - @padding - 20
    y = @_getGameHeight() - @windowHeight - @padding
    @graphics.strokeRect(x, y, 20, 25)

  _createCloseModalButton: ->
    @closeBtn = @scene.make.text
        x: @_getGameWidth() - @padding - 15
        y: @_getGameHeight() - @windowHeight - @padding + 2
        text: 'x'
        style:
          font: 'bold 18px Arial'
          fill: @closeBtnColor

    @closeBtn.setInteractive()
    @closeBtn.on('pointerover', -> @setTint(0xff0000))
    @closeBtn.on('pointerout',  -> @clearTint())
    @closeBtn.on('pointerdown', => @closeWindow())

    @_createCloseModalButtonBorder()

  _createWindow: ->
    @graphics  = @scene.add.graphics()

    gameWidth  = @_getGameWidth()
    gameHeight = @_getGameHeight()
    dimensions = @_calculateWindowDimensions(gameWidth, gameHeight)

    @_createOuterWindow(dimensions...)
    @_createInnerWindow(dimensions...)
    @_createCloseModalButton()
