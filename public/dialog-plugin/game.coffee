preload = ->
  @load.scenePlugin
    key: 'DialogModalPlugin'
    url: './dialog_plugin.js'
    sceneKey: 'dialogModal'

create = ->
  console.log(@dialogModal)
  @dialogModal.init()
  @dialogModal.setText(
    'Lorem ipsum dolor sit amet, consectetur
    adipiscing elit, sed do eiusmod tempor
    incididunt ut labore et dolore magna aliqua.
    Lacus suspendisse faucibus interdum posuere.', true)

game = new Phaser.Game
  type: Phaser.AUTO
  parent: 'phaser-example'
  width: 800
  height: 600
  scene:
    preload: preload
    create:  create
