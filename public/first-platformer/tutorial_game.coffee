preload = ->
  @load.image('sky',        'assets/sky.png')
  @load.image('ground',     'assets/platform.png')
  @load.image('star',       'assets/star.png')
  @load.image('bomb',       'assets/bomb.png')
  @load.spritesheet('dude', 'assets/dude.png', {frameWidth: 32, frameHeight: 48})

createStage = (context) ->
  context.add.image(400, 300, 'sky')

  platforms = context.physics.add.staticGroup()
  platforms
    .create(400, 568, 'ground')
    .setScale(2)
    .refreshBody()
  platforms.create(600, 400, 'ground')
  platforms.create( 50, 250, 'ground')
  platforms.create(750, 220, 'ground')
  return platforms

createPlayer = (context) ->
  player = context.physics.add.sprite(100, 450, 'dude')
  player.setBounce(0.1)
  player.setCollideWorldBounds(true)
  player.score = 0
  player.gameOver = false

  # Setup sprite animations
  context.anims.create
    key: 'left'
    frames: context.anims.generateFrameNumbers('dude', {start: 0, end: 3})
    frameRate: 10
    repeat: -1
  context.anims.create
    key: 'turn'
    frames: [{key: 'dude', frame: 4}]
    frameRate: 20
  context.anims.create
    key: 'right'
    frames: context.anims.generateFrameNumbers('dude', {start: 5, end: 8})
    frameRate: 10
    repeat: -1

  return player

createStars = (context) ->
  stars = context.physics.add.group
    key: 'star'
    repeat: 11
    setXY: {x: 12, y: 0, stepX: 70}

  stars.children
    .iterate((child) -> child.setBounceY(Phaser.Math.FloatBetween(0.4, 0.8)))

  return stars

addBomb = (bombs, aroundPosition) ->
  xPosition =
    if aroundPosition < 400
      Phaser.Math.Between(400, 800)
    else
      Phaser.Math.Between(0, 400)

  bomb = bombs.create(xPosition, 16, 'bomb')
  bomb.setBounce(1)
  bomb.setCollideWorldBounds(true)
  bomb.setVelocity(Phaser.Math.Between(-200, 200), 20)
  bomb.allowGravity = false

respawnAll = (objects) ->
  objects.children
    .iterate((child) -> child.enableBody(true, child.x, 0, true, true))

standStill = (player) ->
  player.setVelocityX(0)
  player.anims.play('turn')

walk = (player, direction, speed=180) ->
  player.anims.play(direction, true)
  switch direction
    when 'left'  then player.setVelocityX(-speed)
    when 'right' then player.setVelocityX(speed)
    else standStill(player)

jump = (player, speed=335) ->
  player.setVelocityY(-speed)

collectStar = (player, star, scoreText, points=10) ->
  # Make star disappear
  star.disableBody(true, true)

  # Update score
  player.score += points
  scoreText.setText("Score: #{player.score}")

hitBomb = (player, scoreText) ->
  scoreText.setText("Game Over! Final Score: #{player.score}")
  player.setTint(0xff0000)
  player.gameOver = true
  standStill(player)

create = ->
  context = this

  # Setup audio
  @audio = new AudioContext()
  @synth = new Tone.Synth().toMaster()

  # Create game objects
  @bombs     = @physics.add.group()
  @platforms = createStage(context)
  @player    = createPlayer(context)
  @stars     = createStars(context)
  @scoreText = @add.text(16, 16, 'Score: 0', {fontSize: '32px', fill: '#000'})

  # Setup controls
  @cursors   = @input.keyboard.createCursorKeys()

  # Event handlers
  onBombHit = (player, bomb) =>
    hitBomb(player, @scoreText)
    # Play death sound
    @audio.resume().then(=> @synth.triggerAttackRelease('D1', '2n'))
    # Halt game
    @physics.pause()

  onStarCollection = (player, star) =>
    collectStar(player, star, @scoreText)
    # Play collection sound
    @audio.resume().then(=> @synth.triggerAttackRelease('A6', '16n'))
    # Add a bomb whenever all visible stars are cleared and make new stars
    if @stars.countActive(true) == 0
      addBomb(@bombs, player.x)
      respawnAll(@stars)

  # Setup physics
  @physics.add.collider(@player, @platforms)
  @physics.add.collider(@stars,  @platforms)
  @physics.add.collider(@bombs,  @platforms)
  @physics.add.collider(@player, @bombs, onBombHit, null, context)
  @physics.add.overlap( @player, @stars, onStarCollection, null, context)

update = ->
  return if @player.gameOver

  # Controls
  if @cursors.left.isDown
    walk(@player, 'left')
  else if @cursors.right.isDown
    walk(@player, 'right')
  else
    standStill(@player)

  if @cursors.up.isDown and @player.body.touching.down
    jump(@player)

game = new Phaser.Game
  type: Phaser.AUTO
  width: 800
  height: 600
  physics:
    default: 'arcade'
    arcade:
      gravity: { y: 330 }
      debug: false
  scene:
    preload: preload
    create:  create
    update:  update
