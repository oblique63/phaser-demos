(function() {
  // From this tutorial:
  // https://www.emanueleferonato.com/2018/05/03/the-basics-behind-jumping-on-enemies-feature-explained-with-phaser-and-arcade-physics-updated-to-phaser-3/
  var PlayGame, PreloadGame, game, gameConfig, gameOptions;

  gameOptions = {
    playerGravity: 1900,
    playerSpeed: 200,
    playerJump: 400,
    enemySpeed: 150
  };

  PreloadGame = class PreloadGame extends Phaser.Scene {
    constructor() {
      super('PreloadGame');
    }

    preload() {
      this.load.tilemapTiledJSON('level', 'level.json');
      this.load.image('tile', 'assets/tile.png');
      this.load.image('hero', 'assets/hero.png');
      return this.load.image('enemy', 'assets/enemy.png');
    }

    create() {
      return this.scene.start('PlayGame');
    }

  };

  PlayGame = class PlayGame extends Phaser.Scene {
    constructor() {
      super('PlayGame');
    }

    create() {
      var enemy_placement, hero_placement, onJump, tile;
      // Load tilemap from 'level'
      this.map = this.make.tilemap({
        key: 'level'
      });
      // Add tile(s) to tilemap
      tile = this.map.addTilesetImage('tileset01', 'tile');
      // Enable collision on tile 1 (the black tile)
      this.map.setCollision(1);
      // Render the first layer as 'layer01'
      this.layer = this.map.createStaticLayer('layer01', tile);
      // Add the hero
      hero_placement = game.config.width / 2;
      this.hero = this.physics.add.sprite(hero_placement, 152, 'hero');
      this.hero.body.gravity.y = gameOptions.playerGravity;
      this.hero.body.velocity.x = gameOptions.playerSpeed;
      // Setup jumping event handler for the hero
      this.hero.canJump = true;
      onJump = () => {
        return this.jump(this.hero);
      };
      this.input.on("pointerdown", onJump, this);
      // Add the enemy
      enemy_placement = game.config.width / 4;
      this.enemy = this.physics.add.sprite(enemy_placement, 152, 'enemy');
      return this.enemy.body.velocity.x = gameOptions.enemySpeed;
    }

    isBlocked(character, direction) {
      return character.body.blocked[direction];
    }

    jump(character) {
      if (character.canJump && this.isBlocked(character, 'down')) {
        return character.body.velocity.y = -gameOptions.playerJump;
      }
    }

    onTileCollision(character, speed) {
      var direction;
      if (this.isBlocked(character, 'right')) {
        character.flipX = true;
      }
      if (this.isBlocked(character, 'left')) {
        character.flipX = false;
      }
      direction = character.flipX ? -1 : 1;
      return character.body.velocity.x = direction * speed;
    }

    onPlayerTileCollision(hero, layer) {
      if (this.isBlocked(hero, 'down')) {
        hero.canJump = true;
      }
      return this.onTileCollision(hero, gameOptions.playerSpeed);
    }

    onEnemyTileCollision(enemy, layer) {
      return this.onTileCollision(enemy, gameOptions.enemySpeed);
    }

    onPlayerEnemyCollision(hero, enemy) {
      // hero is stomping the enemy
      // if the hero is touching DOWN
      // and the enemy is touching UP
      if (enemy.body.touching.up && hero.body.touching.down) {
        return hero.body.velocity.y = -gameOptions.playerJump;
      } else {
        // Game over; restart
        return this.scene.start("PlayGame");
      }
    }

    update() {
      this.physics.world.collide(this.hero, this.layer, this.onPlayerTileCollision, null, this);
      this.physics.world.collide(this.enemy, this.layer, this.onEnemyTileCollision, null, this);
      return this.physics.world.collide(this.hero, this.enemy, this.onPlayerEnemyCollision, null, this);
    }

  };

  gameConfig = {
    type: Phaser.AUTO,
    parent: 'game',
    width: 640,
    height: 192,
    backgroundColor: 0x444444,
    scene: [PreloadGame, PlayGame],
    physics: {
      default: 'arcade',
      arcade: {
        gravity: {
          y: 0
        }
      }
    }
  };

  game = new Phaser.Game(gameConfig);

}).call(this);

//# sourceMappingURL=game.js.map
