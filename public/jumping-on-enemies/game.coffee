# From this tutorial:
# https://www.emanueleferonato.com/2018/05/03/the-basics-behind-jumping-on-enemies-feature-explained-with-phaser-and-arcade-physics-updated-to-phaser-3/

gameOptions =
  playerGravity: 1900
  playerSpeed:    200
  playerJump:     400
  enemySpeed:     150


class PreloadGame extends Phaser.Scene
  constructor: ->
    super('PreloadGame')

  preload: ->
    @load.tilemapTiledJSON('level', 'level.json')
    @load.image('tile',  'assets/tile.png')
    @load.image('hero',  'assets/hero.png')
    @load.image('enemy', 'assets/enemy.png')

  create: ->
    @scene.start('PlayGame')

class PlayGame extends Phaser.Scene
  constructor: ->
    super('PlayGame')

  create: ->
    # Load tilemap from 'level'
    @map = @make.tilemap({key: 'level'})

    # Add tile(s) to tilemap
    tile = @map.addTilesetImage('tileset01', 'tile')

    # Enable collision on tile 1 (the black tile)
    @map.setCollision(1)

    # Render the first layer as 'layer01'
    @layer = @map.createStaticLayer('layer01', tile)

    # Add the hero
    hero_placement = game.config.width / 2
    @hero = @physics.add.sprite(hero_placement, 152, 'hero')

    @hero.body.gravity.y  = gameOptions.playerGravity
    @hero.body.velocity.x = gameOptions.playerSpeed

    # Setup jumping event handler for the hero
    @hero.canJump = true
    onJump = => @jump(@hero)
    @input.on("pointerdown", onJump, this)

    # Add the enemy
    enemy_placement = game.config.width / 4
    @enemy = @physics.add.sprite(enemy_placement, 152, 'enemy')

    @enemy.body.velocity.x = gameOptions.enemySpeed

  isBlocked: (character, direction) ->
    character.body.blocked[direction]

  jump: (character) ->
    if character.canJump and @isBlocked(character, 'down')
      character.body.velocity.y = -gameOptions.playerJump

  onTileCollision: (character, speed) ->
    if @isBlocked(character, 'right')
      character.flipX = true
    if @isBlocked(character, 'left')
      character.flipX = false

    direction = if character.flipX then -1 else 1
    character.body.velocity.x = direction * speed

  onPlayerTileCollision: (hero, layer) ->
    hero.canJump = true if @isBlocked(hero, 'down')
    @onTileCollision(hero, gameOptions.playerSpeed)

  onEnemyTileCollision: (enemy, layer) ->
    @onTileCollision(enemy, gameOptions.enemySpeed)

  onPlayerEnemyCollision: (hero, enemy) ->
    # hero is stomping the enemy
    # if the hero is touching DOWN
    # and the enemy is touching UP
    if enemy.body.touching.up and hero.body.touching.down
      hero.body.velocity.y = -gameOptions.playerJump
    else
      # Game over; restart
      @scene.start("PlayGame")

  update: ->
    @physics.world.collide(@hero,  @layer, @onPlayerTileCollision,  null, this)
    @physics.world.collide(@enemy, @layer, @onEnemyTileCollision,   null, this)
    @physics.world.collide(@hero,  @enemy, @onPlayerEnemyCollision, null, this)


gameConfig =
  type: Phaser.AUTO
  parent: 'game'
  width:  640
  height: 192
  backgroundColor: 0x444444
  scene: [PreloadGame, PlayGame]
  physics:
    default: 'arcade'
    arcade:
      gravity: { y: 0 }

game = new Phaser.Game(gameConfig)
